package main;

public class BezierCurve {

	public BezierCurve() {
	}
	
	private int fact(int n) {
		int prod = 1;
		for(int i=n; i>1; i--) {
			prod *= i;
		}
		return prod; 
	}
	
	private double bernstein(int n, int i, double t) {
		return (fact(n) / (fact(i) * fact(n-i))) * Math.pow(1-t, n-i) * Math.pow(t, i);
	}
	
	public void bezier2D(double[] b, int cpts, double[] p) {
	    int npts = (b.length) / 2;
	    int cIndex, bIndex;
	    double step, t;

	    cIndex = 0;
	    t = 0;
	    step = (double)1.0 / (cpts - 1);

	    for (int i1 = 0; i1 != cpts; i1++) {
	        if ((1.0 - t) < 5e-6) 
	            t = 1.0;

	        bIndex = 0;
	        p[cIndex] = 0.0;
	        p[cIndex + 1] = 0.0;
	        for (int i = 0; i != npts; i++) {
	            double basis = bernstein(npts - 1, i, t);
	            p[cIndex] += basis * b[bIndex];
	            p[cIndex + 1] += basis * b[bIndex + 1];
	            bIndex = bIndex + 2;
	        }

	        cIndex += 2;
	        t += step;
	    }
	}
}
