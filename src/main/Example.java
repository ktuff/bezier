package main;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class Example {
	
	public static void main(String[] args) {
		
		JFrame frame = new JFrame("");

		int width = 800, height = 600;
	    frame.setSize(width, height);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setLocationRelativeTo(null);
	    frame.setResizable(false);
	    frame.setVisible(true);

	    Canvas canvas = new Canvas();
	    canvas.setSize(width, height);
	    canvas.setBackground(Color.BLACK);
	    canvas.setVisible(true);
	    canvas.setFocusable(false);

	    frame.add(canvas);
	    canvas.createBufferStrategy(3);
	    
	    boolean running = true;

	    BufferStrategy bufferStrategy;
	    Graphics graphics;
	    BezierCurve curve = new BezierCurve();
	    int pts = 1000;
	    double[] b = {0.3, 0, 0.4, 0, 0.44, 0, 0.5, 0.05, 0.6, 0.15, 0.72, 0.5, 0.84, 0.85, 0.94, 0.95, 1.0, 1, 1.04, 1, 1.14, 1};
	    double[] p = new double[pts*2];
	    curve.bezier2D(b, pts, p);

	    while (running) {
	        bufferStrategy = canvas.getBufferStrategy();
	        graphics = bufferStrategy.getDrawGraphics();
	        graphics.clearRect(0, 0, width, height);

	        graphics.setColor(Color.GREEN);
	        int x0=0, x1=0, y0=0, y1=0;
	        for(int i=0; i<pts*2; i+=2) {
	        	if(p[i] > 1.0) {
	        		break;
	        	}
	        	x1 = (int)(p[i]*(width-10));
	        	y1 = (int)(p[i+1]*(height-30));
		       	graphics.drawLine(x0, y0, x1, y1);
		        x0 = x1;
		        y0 = y1;
	        }
	        graphics.drawLine((int)(0.44*(width-10)), 0, (int)(0.44*(width-10)), height);
	        
	        bufferStrategy.show();
	        graphics.dispose();
	    }
	}
}
